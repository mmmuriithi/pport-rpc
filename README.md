# README

Pport RPC Package : Easily fluent remote calls to your backend.

# Installation

Install pport\rpc using composer :

    composer require pport/rpc

## Deploy A Server and Client

#### 1. Deploy the server : server.php

```php
<?php

class TestRemoteClass1
{
    public static $api_data;
    public function helloWorld()
    {
        return ['attribute1' => 'TestRemoteClass1 Attribute 1', 'attribute2' => 'TestRemoteClass1 Attribute 2'];
    }
}


class TestRemoteClass2
{
    public static $api_data;
    public function fetchData()
    {
        /**return [
            'data' => ['1', '2', '3', '4']
        ];**/

        return ['1', '2', '3', '4'];
    }

    public function fetchString()
    {
        return "HelloWorld 2";
    }
}

//Run the server
Pport\Rpc\Server::run();
/**Pport\Rpc\Server::verify_request(function (){
    Pport\Rpc\Server::run();
})**/;?>
```

#### 2. Connect to your server from your frontend or other interface : e.g. client.php

```php
<?php
    $rpcClient = new Pport\Rpc\Client();
    $rpcClient->set_api_url("http://localhost/epesicloud/pport-rpc/tests/server.php");
    //Call remote class method
    $testRemoteObject1 = TestRemoteClass1::helloWorld();
    //Access returned data  attributes from the returned object/s
    echo $testRemoteObject1->attribute1;
    echo "<br/>";
    echo $testRemoteObject1->attribute2;
    echo "<br/>";
    echo "<br/>";
    //Access returned data  attributes from the returned object/s
    $testRemoteObject2 = TestRemoteClass2::fetchData();
    var_dump($testRemoteObject2);

    //Access String Data
    $fetchedString = TestRemoteClass2::fetchString();
    var_dump($fetchedString);
;?>
```

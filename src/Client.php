<?php

namespace pPort\Rpc;

/**
 * A PHP Consumer Library for Consumer,You can create implementations for Java,Python,Javascript etc..based on this implementation
 * Gives you access to all that you need from Consumer
 *
 * Use me for good and not Evil!!!!!
 *
 *
 * @author <Martin Muriithi> <martin@pporting.org>
 *
 */
class Client
{
    //App key assigned to the consumer...
    public $app_key;
    //App secret yo be used by consumer...
    public $app_secret;
    //What action to query..
    public $action;
    //Set the account to fetch data for....each account will be having a set of  authenticated...assigned consumers
    public $account = "demo";
    //Login url to be used, where clients to login for request token authorization
    public $login_url = "";
    //Should be the url of the api

    public $request_type;

    public $entity = FALSE;

    public $decode = TRUE;

    public $mode = 'desktop';

    public $api_url;

    public $entities = [];

    public $loaded_entities = [];

    public $entities_data = [];

    public $cache_enabled = [];

    public $response_data = [];

    public function __construct($app_key = FALSE, $app_secret = FALSE)
    {
        $this->app_key = $app_key;
        $this->app_secret = $app_secret;
        spl_autoload_register(array($this, 'call_entity'), false, false);
    }

    public function enable_cache()
    {
        $this->cache_enabled = true;
    }

    public function get_session_data()
    {
        return isset($this->response_data[$this->entity]) ? $this->response_data[$this->entity] : [];
    }


    public function curl_json($url, $json_string, $type = "POST")
    {

        if (is_array($json_string) && isset($json_string['headers'])) {
            $header_data = $json_string['headers'];
            $json_string['headers']['Content-Type'] = "application/json";
            foreach ($header_data as $header_k => $header_v) {
                $headers[] = $header_k . ": " . $header_v;
            }
            unset($json_string['headers']);
        } else {
            $headers[] = 'Content-Type: application/json';
        }
        $json_string = is_array($json_string) || is_object($json_string) ? json_encode($json_string) : $json_string;
        $headers[] = 'Content-Length: ' . strlen($json_string);

        $ch = curl_init($url);


        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $type);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);

        curl_close($ch);
        return $result;
    }

    public function call_entity($class)
    {
        if (!isset($this->called_entities[$class])) {
            eval('class ' . $class . ' { 
                public static $api_service=NULL;
               
                

                public static function enable_cache()
                {
                    call_user_func_array(array(static::$api_service,"enable_cache"),[]);
                }

                public static function clone_result_object($result)
                {
                    if(is_array($result)||is_object($result))
                    {
                        $result_object=new static();
                        foreach($result as $key=>$val)
                        {
                            $result_object->{$key}=$val;
                        }
                        return $result_object;
                    }
                    return $result;
                    
                }

                public static function clone_results($result)
                {
                    $clone_result=[];
                   if(is_object($result))
                   {
                        $clone_result=static::clone_result_object($result);
                   }
                   else
                   {
                       if(is_array($result))
                       {
                            foreach($result as $result_object)
                            {
                                $clone_result[]=static::clone_result_object($result_object);
                            }
                       }
                       else
                       {
                            $clone_result=$result;
                       }
                        
                   }
                   return $clone_result;

                    
                }

                public static function __callStatic($method, $arguments)
                {
                    static::$api_service->entity=get_class();
                    $data=$arguments;
                   
                    $response_result=call_user_func_array(array(static::$api_service,$method),$data);
                    $cloned_result=static::clone_results($response_result);
                    if(is_object($cloned_result))
                    {
                        static::$api_service->response_data[static::$api_service->entity]=$cloned_result->get_data();
                    }
                    
                    return $cloned_result;
                }

                public function __call($method, $arguments)
                {
                    static::$api_service->entity=get_class();
                    $args_data=[];
                    $data=$arguments;
                   
                    $response_result=call_user_func_array(array(static::$api_service,$method),$data);
                    return $response_result;
                }

                public function get_data()
                {
                    return get_object_vars($this);
                }

                public function __get($var)
                {
                    
                    static::$api_service->entity=get_class();
                    $response_result=call_user_func_array(array(static::$api_service,$var),[]);                   
                    return $response_result;
                }
            }');

            $class::$api_service = $this;
            $this->loaded_entities[$class] = $class;
        }
    }

    public function set_api_url($api_url)
    {
        $this->api_url = $api_url;
    }

    public function set_entity($entity)
    {
        $this->api_url = $entity;
    }


    //Set's the call_back.
    public function call_back()
    {
    }





    public function request($url, $params = array())
    {
        $ch = curl_init();
        $params['mode'] = $this->mode;
        $params['account'] = $this->account;

        /**if(isset($_COOKIE['PHPSESSID']))
        {
            $strCookie = 'PHPSESSID=' . $_COOKIE['PHPSESSID'] . '; path=/';
            session_write_close();
            curl_setopt( $ch, CURLOPT_COOKIE, $strCookie );
        }**/

        $params['session_data'] = $this->get_session_data();


        $server_output = $this->curl_json($url, $params);

        return $server_output;

        $params = http_build_query($params);

        $url = ($this->request_type === "GET" || $this->request_type == 'FIND') ? $url . "?" . $params : $url;
        curl_setopt($ch, CURLOPT_URL, $url);
        if ($this->request_type === "POST") {
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        }
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $server_output = trim(curl_exec($ch));

        curl_close($ch);


        return $server_output;
    }

    //Will be used to authorize request token...
    public function authorize_token($request_token)
    {
        ///Authorize the  request_token
    }
    //Get action with the access token
    public function execute_request($access_token)
    {
        //Execute request with an access_token
    }

    public function get_request_type($func)
    {
        $request_func = explode("_", $func);
        $request_type = $request_func[0];
        if ($this->is_valid_request($request_type)) {
            return $request_type;
        }
        return "get";
    }

    public function parse_api_result($api_result)
    {

        if ($this->cache_enabled) {
            $this->entities_data[$this->entity]['api_result'] = $api_result;
        }
        if ($this->decode == TRUE) {
            $parsed_server_output = (($decoded = json_decode($api_result)) != null) ? $decoded : $api_result;
            $parsed_server_output = $parsed_server_output == "[]" ? NULL : $parsed_server_output;
        }

        if (!is_object($parsed_server_output)) {
            echo $api_result;
        }

        return ((is_object($parsed_server_output)) && property_exists($parsed_server_output, "data")) ? $parsed_server_output->data : $api_result;
    }


    public function fetch_from_cache($func, $args)
    {
        if (isset($this->entities_data[$this->entity])) {


            if (
                isset($this->entities_data[$this->entity]['func'])
                && $this->entities_data[$this->entity]['func'] == $func
            ) {
                if (
                    isset($this->entities_data[$this->entity]['args'])
                    && $this->entities_data[$this->entity]['args'] == $args
                ) {
                    $cache_result = $this->entities_data[$this->entity]['api_result'];
                    return $this->parse_api_result($cache_result);
                }
            }
        }
        return NULL;
    }

    public function __call($func, $args)
    {


        if ($this->cache_enabled) {

            if ($cache_result = $this->fetch_from_cache($func, $args)) {
                //Check Cache

                return $cache_result;
            } else {
                $this->entities_data[$this->entity] = [
                    'func' => $func,
                    'args' => $args
                ];
            }
        }


        $request_type = $this->get_request_type($func);
        $this->request_type = strtoupper($request_type);
        $this->action = $func;
        $url = $this->get_api_url() . "?entity=" . $this->entity . "&action=" . $this->action;
        /**if(isset($args[0]) && is_array($args[0]))
        {
            $args=$args[0];
        } **/
        $api_result = call_user_func_array(array($this, "request"), array($url, $args));


        return $this->parse_api_result($api_result);
    }

    public function get_api_url()
    {
        return $this->api_url;
    }

    public function is_valid_request($request)
    {
        return in_array($request, array("get", "post", "find", "GET", "POST"));
    }
}

/**
 Sample profiling : Showing improvement (1,312 times better) of performance when cache enabled
 profiler_flag("Start");
Client::enable_cache();
profiler_flag("Client Without Cache");
$user=Client::get_client_by_phone(254729934932);
profiler_flag("Client With Cache");
$user=Client::get_client_by_phone(254729934932);
profiler_flag("Done");
profiler_print();
 **/
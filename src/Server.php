<?php

namespace pPort\Rpc;

/**
 * An Oauth provider class to Server consumers...
 *
 * @author <Martin Muriithi> <martin@pporting.org>
 *
 */


class Server
{

    public static $entity = false;
    public static $resource_action = false;

    public function __construct($entity = FALSE, $resource = FALSE)
    {
        static::$entity = $entity;
        static::$resource_action = strtolower($_SERVER['REQUEST_METHOD']) . "_" . $resource;
        $this->run();
    }

    public static function __callStatic($entity, $args)
    {
        $resource_action = isset($args[0]) ? $args[0] : FALSE;
        $params = isset($args[1]) ? $args[1] : array();
        $decode = isset($args[2]) ? $args[2] : FALSE;
        return static::run($entity, $resource_action, $params, $decode);
    }

    public static function unset_keywords(&$params)
    {
        $keys = array_keys(['keyword1']);
        foreach ($keys as $key) {
            unset($params[$key]);
        }
    }

    public static function is_assoc($arr)
    {
        // don't try to check non-arrays or empty arrays
        if (FALSE === is_array($arr) || 0 === ($l = count($arr))) {
            return false;
        }

        // shortcut by guessing at the beginning
        reset($arr);
        if (key($arr) !== 0) {
            return true;
        }

        // shortcut by guessing at the end
        end($arr);
        if (key($arr) !== $l - 1) {
            return true;
        }

        // rely on php to optimize test by reference or fast compare
        return array_values($arr) !== $arr;
    }

    public static function  arr($key, $array_or_default, $default = false, $if_true_return = false)
    {
        if (is_object($array_or_default)) {
            return (isset($array_or_default->{$key})) ? $array_or_default->{$key} : $default;
        } elseif (is_string($array_or_default)) {

            return (class_exists($array_or_default) && isset($array_or_default::$$key)) ? $array_or_default::$$key : $default;
        } else {
            return ($key) ? (is_array($array_or_default) && array_key_exists($key, $array_or_default) ? ($if_true_return) ?: $array_or_default[$key] : $default) : false;
        }
    }


    final public static function run($entity = FALSE, $resource_action = FALSE, $params = array(), $decode = FALSE)
    {

        $entity = static::arr("entity", $_GET, $entity);
        $resource_action = static::arr("action", $_GET, $resource_action);


        $json_data = json_decode(file_get_contents("php://input"), true);
        if (($json_data) && is_array($json_data)) {
            $request_data = array_merge($_REQUEST, $json_data);
        } else {
            $request_data = $_REQUEST;
        }

        if (isset($_GET['entity'])) {
            unset($request_data['entity']);
            unset($request_data['action']);
        }


        $params = count($params) ? $params : $request_data;

        $entity = (static::$entity === false) ? $entity : static::$entity;
        $resource_action = (static::$resource_action === false) ? $resource_action : static::$resource_action;
        if (isset($params['url'])) {
            unset($params['url']);
        }

        if ($entity) {
            $action_params = $params;
            if (isset($action_params["params"]) && count($action_params) > 0) {
                $action_params[0] = $action_params['params'];
            }
            /** FIX : MORE NATURAL WAY OF CALLS 
            if(isset($action_params[1]) && is_assoc($action_params[1]))
            {
                $action_params=array_merge($action_params,$action_params[1]);                
            }
             **/

            //Unset unrequired vars
            //static::unset_keywords($action_params);
            foreach (['mode', 'url',  'account', 'params'] as $system_param) {
                if (isset($action_params[$system_param])) {
                    unset($action_params[$system_param]);
                }
            }



            $id = NULL;
            $session_data = [];

            if (isset($action_params['session_data'])) {
                $session_data = $action_params['session_data'];
                unset($action_params['session_data']);
            }

            if (isset($action_params[0]) && static::is_assoc($action_params[0])) {
                //Calls with an associative array will result 
                $id = static::arr('id', $action_params[0], NULL);
            }

            if (!$id) {
                $id = static::arr('id', $session_data, NULL);
            }

















            $result = NULL;





            try {

                if (method_exists($entity, $resource_action)) {


                    $method_checker = new \ReflectionMethod($entity, $resource_action);

                    if ($method_checker->isStatic()) {


                        if (empty($action_params)) {
                            $result = $entity::$resource_action();
                        } else {


                            //$result=call_user_func_array(array($entity,$resource_action),$action_params);


                            /** FIX : MORE NATURAL WAY OF CALLS, ACTIVE RECORD CALLS **/

                            if (static::is_assoc($action_params)
                                /**  && (
                                     array_key_exists("conditions",$action_params)
                                     ||
                                     array_key_exists("count",$action_params)
                                     ||
                                     array_key_exists("group_by",$action_params)
                                     ||
                                     array_key_exists("order",$action_params)
                                    )
                             */
                            ) {


                                $result = call_user_func_array(array($entity, $resource_action), [$action_params]);
                            } else {
                                $result = call_user_func_array(array($entity, $resource_action), $action_params);
                            }
                        }
                    } else {

                        if (isset($id)) {

                            $entity_obj = $entity::find($id);
                        } else {
                            $entity_obj = new $entity;
                        }

                        $result = call_user_func_array(array($entity_obj, $resource_action), $action_params);
                        /** FIX : MORE NATURAL WAY OF CALLS if(static::is_assoc($action_params))
                            {
                                    $result=call_user_func_array(array($entity_obj,$resource_action),[$action_params]);
                            }
                            else
                            {
                                    $result=call_user_func_array(array($entity_obj,$resource_action),$action_params);
                            }**/
                    }
                } else {
                    if ($id) {
                        $result = $entity::find($id);
                    } else {

                        $result = call_user_func_array(array($entity, $resource_action), $action_params);
                    }
                }
            } catch (\Exception $e) {
                $result = NULL;
                $error_message = $e->getMessage();
            }



            /**The relations allways have to be parsed to cater for relation objects child-realtions in-case of a call to child-relation
                In this scenario, $entity is instead changed to the relation object class_name as per the logic after method_exists**/
            if (is_object($result)) {

                $entity_object = $result;


                $merged_relations = [];
                foreach (["has_many", "belongs_to", "has_one"] as $relation_type) {

                    if (isset($entity::$$relation_type)) {
                        $relation_data = $entity::$$relation_type;
                        foreach ($relation_data as $relation_config) {
                            $relation_config['relation_type'] = $relation_type;
                            $merged_relations[$relation_config[0]] = $relation_config;
                        }
                    }
                }


                //First Check if relation was requested as the resource action and return the result
                $parsed_resource_action = str_replace("get_", "", $resource_action);

                $relation_as_action = static::arr($parsed_resource_action, $merged_relations, NULL);

                if ($relation_as_action) {

                    $entity_results = $entity_object->{$parsed_resource_action};

                    if ($relation_as_action['relation_type'] == "has_many") {
                        if ($entity_results) {
                            foreach ($entity_results as $result) {
                                $results_array[] = $result->to_array();
                            }
                            $result = $entity_results;
                        } else {
                            $result = [];
                        }
                    } else {
                        if ($entity_results) {

                            $result = $entity_results;
                        } else {
                            $empty_relation_object = new $relation_as_action['class_name'];
                            $result = $empty_relation_object;
                        }
                    }
                } else {

                    if (method_exists($result, "get_" . $resource_action)) {

                        $method_action = "get_" . $resource_action;
                        $result = $result->$method_action();
                    }
                }
            }
        } else {

            $result = static::_error_temp("The entity '{$entity}' is Invalid");
        }
        if (is_array($result) && isset($result[0])) {

            foreach ($result as $record) {
                if (method_exists($record, "to_array")) {
                    $records[] = $record->to_array();
                }
            }
            $result = $records;
        } elseif (is_object($result) && method_exists($result, "to_array")) {

            $entity_object = $result;
            $result = $result->to_array();
            if (isset($action_params['show_relations']) || isset($relations_requested[0])) {
                foreach ($merged_relations as $relation_name => $relation_config) {
                    if ($relation_config['relation_type'] == "has_many") {
                        if ($entity_object->{$relation_name}) {
                            $relation_results = [];
                            foreach ($entity_object->{$relation_name} as $relation_result) {
                                $relation_results[] = $relation_result->to_array();
                            }
                            $result[$relation_name] = $relation_results;
                        } else {
                            $result[$relation_name] = [];
                        }
                    } else {
                        if ($entity_object->{$relation_name}) {
                            $result[$relation_name] = $entity_object->{$relation_name}->to_array();
                        } else {
                            $empty_relation_object = new $relation_config['class_name'];
                            $result[$relation_name] = $empty_relation_object->to_array();
                        }
                    }
                }
            }
        }

        $info = $result ? "success" : "error";
        $message = isset($error_message) ? $error_message : "Success";
        static::json_render(["info" => $info, "data" => $result, 'message' => $message]);
    }

    public static function json_render($arr)
    {
        header('content-type: application/json; charset=utf-8');
        if (is_string($arr)) {
            exit($arr);
        }
        exit(json_encode($arr));
    }




    private static function _error_temp($error)
    {
        return (array("info" => "error", "message" => $error, "data" => NULL));
    }

    private static function _success_temp($result)
    {
        return (array("info" => "success", "entity" => static::$entity, "resource_action" => static::$resource_action, "message" => "Request Successful", "data" => $result));
    }
}